import { Injectable } from '@angular/core';
import { BehaviorSubject,Subject ,Observable} from 'rxjs';

@Injectable()
export class DataService {

    private myMessage = new BehaviorSubject<boolean>(false);
  

  constructor() { }
  getMessage(): Observable<boolean> {
    return this.myMessage.asObservable();
 }
 updateMessage(message: boolean) {
    this.myMessage.next(message);
  }

  

}