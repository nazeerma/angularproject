import { TestBed } from '@angular/core/testing';

import { UwprofileService } from './uwprofile.service';

describe('UwprofileService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UwprofileService = TestBed.get(UwprofileService);
    expect(service).toBeTruthy();
  });
});
