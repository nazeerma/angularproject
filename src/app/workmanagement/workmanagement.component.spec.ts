import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkmanagementComponent } from './workmanagement.component';

describe('WorkmanagementComponent', () => {
  let component: WorkmanagementComponent;
  let fixture: ComponentFixture<WorkmanagementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkmanagementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkmanagementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
