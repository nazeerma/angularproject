import { Component, OnInit } from '@angular/core';
//import uwplstData from './uwplist.json';
import {ConfigService} from './uwprofileview.service';
import { DataService } from "../services/datapassing";
import {  Router, ActivatedRoute, ParamMap } from '@angular/router';
 
@Component({
  selector: 'app-uwprofileoverview',
  templateUrl: './uwprofileoverview.component.html',
  styleUrls: ['./uwprofileoverview.component.css'],
  providers:[ConfigService]
})
export class UwprofileoverviewComponent  implements OnInit {
  title = 'UW Profile Overview';
  Uwprofiles:any;
  addformvalue:any;
  customerData:any;
 // Uwprofiles: any =uwplstData;
  constructor(private configService:ConfigService ,private messageService:DataService,private router:Router) {
    this.showConfig();
    this.addformvalue=false;
   }
   adduwprofile()
   {
    let targetvalue:any =event.target;

    if (targetvalue.value =="View")
    {
     this.addformvalue=true;
    }
    else{
     this.addformvalue=false;
    }
    this.router.navigate(['/adduwprofile']);
    this.messageService.updateMessage(this.addformvalue);
     
   }
   addform()
   {
     
     
   }
  showConfig() {
    this.configService.getConfig()
      .subscribe((data: any) =>  {
       this.Uwprofiles=data.collection;
         
      });
  }

  ngOnInit() {
    //this.datservice.currentMessage.subscribe(mes=> this.addformvalue=mes )
  }

}