import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UwprofileoverviewComponent } from './uwprofileoverview.component';

describe('UwprofileoverviewComponent', () => {
  let component: UwprofileoverviewComponent;
  let fixture: ComponentFixture<UwprofileoverviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UwprofileoverviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UwprofileoverviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
