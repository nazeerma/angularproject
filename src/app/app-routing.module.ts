import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdduwprofileComponent } from './adduwprofile/adduwprofile.component';
import { UwprofileoverviewComponent } from './uwprofileoverview/uwprofileoverview.component';
import { WorkmanagementComponent } from './workmanagement/workmanagement.component';


const routes: Routes = [
  {path:'uwlist',component:UwprofileoverviewComponent},
  {path:'adduwprofile',component:AdduwprofileComponent},
  {path:'',component:WorkmanagementComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
