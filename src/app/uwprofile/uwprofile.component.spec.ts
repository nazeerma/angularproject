import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UwprofileComponent } from './uwprofile.component';

describe('UwprofileComponent', () => {
  let component: UwprofileComponent;
  let fixture: ComponentFixture<UwprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UwprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UwprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
