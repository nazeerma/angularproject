import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdduwprofileComponent } from './adduwprofile.component';

describe('AdduwprofileComponent', () => {
  let component: AdduwprofileComponent;
  let fixture: ComponentFixture<AdduwprofileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdduwprofileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdduwprofileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
