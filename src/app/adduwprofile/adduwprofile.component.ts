import { Component, OnInit } from '@angular/core';
import { DataService } from "../services/datapassing";
import { Subscription } from 'rxjs';
import {  Router, ActivatedRoute, ParamMap } from '@angular/router';

@Component({
  selector: 'app-adduwprofile',
  templateUrl: './adduwprofile.component.html',
  styleUrls: ['./adduwprofile.component.css'],
 
})
export class AdduwprofileComponent implements OnInit {
  disabledValue:boolean;
  message:string;
  subscription:Subscription;
  messageFromSibling:any;
  showform:boolean=false;
  

  constructor(private messageService:DataService ,private route:Router) { 
  
  }
  cancelform(eve)
  {
    this.showform=!this.showform
    if (!eve)
    {
     this.route.navigate(['/uwlist'])
    }
  }

  ngOnInit() {
    this.subscription = this.messageService.getMessage()
    .subscribe(mymessage => this.disabledValue = mymessage)

  }
  ngOnDestroy() {
    this.subscription.unsubscribe();
  }

}
