import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UwprofileoverviewComponent } from './uwprofileoverview/uwprofileoverview.component';
import { UwprofileComponent } from './uwprofile/uwprofile.component';
import { FormsModule }   from '@angular/forms';
import { FilterPipe} from './pipes/uwprofile.search';
import { AdduwprofileComponent } from './adduwprofile/adduwprofile.component';
import { DataService } from "./services/datapassing";
import { WorkmanagementComponent } from './workmanagement/workmanagement.component';

@NgModule({
  declarations: [
    AppComponent,
    UwprofileoverviewComponent,
    UwprofileComponent,
    FilterPipe,
    AdduwprofileComponent,
    WorkmanagementComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,HttpClientModule,
    FormsModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
